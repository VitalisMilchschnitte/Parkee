<%@page import="controller.ResidentParkingLotManager"%>
<%@page import="model.Owner"%>

<!DOCTYPE HTML>

<!-- checks if the user is logged in as owner -->
<jsp:include page="owner_CheckLogin.jsp"></jsp:include>

<%
	Owner owner = (Owner) request.getSession().getAttribute("owner");
	String postCode = request.getParameter("postCode");
	String postCodeDistrict = request.getParameter("postCodeDistrict");

	ResidentParkingLotManager rplm = new ResidentParkingLotManager();
	rplm.registerResidentParkingLot(owner, postCode, postCodeDistrict);
%>

<jsp:forward page="owner_ParkingLot.jsp"></jsp:forward>
