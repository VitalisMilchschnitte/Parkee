<%@page import="model.Customer"%>

<!Doctype HTML>

<%-- checks if the customer is logged in --%>
<%
	Customer customer = (Customer) request.getSession().getAttribute("customer");
	if (customer == null){
		%>
			<jsp:forward page="frontpage.html"></jsp:forward>
		<%
	}
%>