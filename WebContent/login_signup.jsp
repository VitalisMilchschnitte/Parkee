<html>
<head>
	<meta charset="UTF-8" />
	
	<title>Login / Registrierung</title>
	
	<link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script src="vendor/jquery/jquery.js" type="text/javascript"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- Custom styles for this template-->
	<link href="vendor/css/sb-admin.css" rel="stylesheet">
	<link href="vendor/css/login.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,
	400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
	
	<script type="text/javascript">
		// Check if the password is correct in the verify input
		function checkPass() {
			var pass1 = document.getElementById('passwd');
			var pass2 = document.getElementById('passwd_confirm');	
			var message = document.getElementById('message');	
			var colorRight = "#66cc66";
			var colorWrong = "#ff6666";
	
			if (pass1.value.length && pass2.value.length > 5) {
				if (pass1.value == pass2.value) {
					pass2.style.backgroundColor = colorRight;
					pass1.style.backgroundColor = colorRight;
					message.style.color = colorRight;
					message.innerHTML = "Passwort stimmt �berein!"
					$(":button_customer").show();
					$(":button_owner").show();
	
				} else {
					pass2.style.backgroundColor = colorWrong;
					pass1.style.backgroundColor = colorWrong;
					message.style.color = colorWrong;
					message.innerHTML = "Passwort stimmt nicht �berein!";
					$(":button_customer").hide();
					$(":button_owner").hide();
				}
			} else {
				message.innerHTML = "Passwort muss mindestens 6 Zeichen lang sein!"
				message.style.color = colorWrong;
			}
		}
	</script>
</head>

<body>
	<div class="container">
		<header>
			<h1>Login / Registrierung</h1>
		</header>
		<section>
			<div id="container_demo">
				<!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
				<a class="hiddenanchor" id="toregister"></a> <a class="hiddenanchor"
					id="tologin"></a>
				<div id="wrapper">
					<div id="login" class="animate form">
						<form action="login.jsp" autocomplete="on" method="post">
							<h1>Login</h1>
							<p>
								<label for="username" class="uname"> Emailadresse
									eingeben </label> <input id="email" name="email" required="required"
									type="text" placeholder="z.B. MaxMustermann@gmail.com" />
							</p>
							<p>
								<label for="password" class="youpasswd"> Ihr Passwort </label> <input
									id="passwd1" name="passwd1" required="required" type="password" />
							</p>
							<p class="keeplogin">
								<input type="checkbox" name="loginkeeping" id="loginkeeping"
									value="loginkeeping" /> <label for="loginkeeping">Eingeloggt
									bleiben..</label>
							</p>
							<p class="login button">
								<input type="submit" value="Login" target="_blank">
							</p>
							<p class="change_link">
								Noch nicht registriert? <a href="#toregister"
									class="to_register">Registrieren</a>
							</p>
						</form>
					</div>

					<div id="register" class="animate form">
						<form name="register" autocomplete="on" method="post" action="">
							<h1>Registrieren</h1>
							<p>
								<label for="usernamesignup" class="uname">Benutzername
									angeben</label> <input id="name" name="name" required="required"
									type="text" placeholder="MaxMustermann" />
							</p>
							<p>
								<label for="emailsignup" class="youmail">Emailadresse</label> <input
									id="email" name="email" required="required" type="email"
									placeholder="MaxMustermann@gmail.com" />
							</p>
							<p>
								<label for="passwd" class="youpasswd">Passwort</label> <input
									id="passwd" name="passwd" required="required" type="password"
									onKeyUp='checkPass();' />
							</p>
							<p>
								<label for="passwd_confirm" class="youpasswd">Passwort
									best&auml;tigen</label> <input id="passwd_confirm"
									name="passwd_confirm" required="required" type="password"
									onKeyUp='checkPass();' />
							</p>
							<p>
								<span id='message'></span>
							</p>
							<p>
								<label for="state" class="state"> Registrieren als
									Parkplatz-... </label> <br>
							</p>
							<p class="submit button">
								<input type="submit" formmethod="post" id="button_customer"
									formaction="customer_SignUp.jsp" value="Nutzer"
									style="margin-right: 60px; margin-left: 30px"> <input
									type="submit" formmethod="post" id="button_owner"
									formaction="owner_SignUp.jsp" value="Betreiber">
							</p>
							<p class="change_link">
								Schon registriert? <a href="#tologin" class="to_register">Einloggen</a>
							</p>
						</form>
					</div>

				</div>
			</div>
		</section>
	</div>
</body>

</html>