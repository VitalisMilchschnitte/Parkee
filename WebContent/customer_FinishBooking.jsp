<%@page import="controller.VehicleManager"%>
<%@page import="controller.ResidentAreaManager"%>
<%@page import="controller.CustomerManager"%>
<%@page import="model.ResidentArea"%>
<%@page import="model.Customer"%>
<%@page import="model.Vehicle"%>
<%@page import="controller.ResidentBookingManager"%>

<h1> Buchung erfolgreich!</h1>

<%
	ResidentBookingManager rbm = new ResidentBookingManager();		
	
	Customer customer1 = (Customer) request.getSession().getAttribute("customer");
	int customer3 = customer1.getId();
	
	int vehicleId = Integer.parseInt(request.getParameter("vehicleId"));
	int residentAreaId = Integer.parseInt(request.getParameter("residentAreaId"));
	String paymentMethod = request.getParameter("paymentMethod");
	String duration = request.getParameter("duration");
	
	VehicleManager vm = new VehicleManager();
	ResidentAreaManager ram = new ResidentAreaManager();
	CustomerManager cm = new CustomerManager();
	
	Customer customer = cm.readCustomerByID(customer3);
	Vehicle vehicle = vm.readVehicleByID(vehicleId);
	ResidentArea residentArea = ram.readResidentAreaByID(residentAreaId);
	
	if(duration.equals("year")){
		rbm.bookResidentAreaForOneYear(customer, vehicle, residentArea, paymentMethod);
	}
	if(duration.equals("month")){
			rbm.bookResidentAreaForOneMonth(customer, vehicle, residentArea, paymentMethod);
	}
	if(duration.equals("week")){
			rbm.bookResidentAreaForOneWeek(customer, vehicle, residentArea, paymentMethod);
	}
%>

<jsp:forward page="customer_Bookings.jsp"></jsp:forward>