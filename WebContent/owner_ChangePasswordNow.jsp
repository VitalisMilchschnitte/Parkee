<%@page import="controller.OwnerManager"%>
<%@page import="model.Owner"%>

<html>
<head>
	<script src="vendor/jquery/jquery.js" type="text/javascript"></script>
	<link href="vendor/css/stylish-portfolio.css" rel="stylesheet" type="text/css">
</head>

<jsp:include page="owner_CheckLogin.jsp"></jsp:include>

<%
	Owner owner = (Owner) request.getSession().getAttribute("owner");
	String newPassword = request.getParameter("passwd");

	OwnerManager om = new OwnerManager();
	om.updateOwnerPassword(owner, newPassword);
%>

<jsp:forward page="owner_Profile.jsp"></jsp:forward>

</html>