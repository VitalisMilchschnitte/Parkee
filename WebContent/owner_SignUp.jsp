<%@page import="java.awt.Button"%>
<%@page import="model.Customer"%>
<%@page import="controller.CustomerManager"%>
<%@page import="controller.OwnerManager"%>
<%@page import="qrCode.QRCodeGenerator"%>
<%@page import="controller.RegisterManager"%>

<html>
<head>
	<script src="vendor/jquery/jquery.js" type="text/javascript"></script>
	<link href="vendor/css/stylish-portfolio.css" rel="stylesheet" type="text/css">
</head>

<%
	String name = request.getParameter("name");
	String email = request.getParameter("email");
	String passwd = request.getParameter("passwd");

	RegisterManager rm = new RegisterManager();
	boolean emailUsed = rm.isEmailUsed(email);

	if (emailUsed == true) {
		%>
			<div class="alert alert-danger">
				<h3 style="color: red"><strong>Achtung!</strong> Diese E-mail-Adresse ist bereits in unserem System vergeben.</h3>
		 		<p style="font-size: 20">Bitte geben Sie eine andere E-mail-Adresse ein.</p>
	 		</div>
		<%
	} else {
		rm.registerOwner(name, email, passwd);
		%>
			<jsp:forward page="login_signup.jsp"></jsp:forward>
		<%
	}
%>

</html>