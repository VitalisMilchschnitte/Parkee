<%@page import="controller.VehicleManager"%>
<%@page import="model.Vehicle"%>

<!DOCTYPE HTML>

<!-- checks if the user is logged in as customer -->
<jsp:include page="customer_CheckLogin.jsp"></jsp:include>

<%
	String vehicleIDinput = request.getParameter("vehicleID");
	int vehicleID = Integer.parseInt(vehicleIDinput);
	
	VehicleManager vm = new VehicleManager();
	vm.deleteVehicleByID(vehicleID);
%>

<jsp:forward page="customer_Vehicles.jsp"/>