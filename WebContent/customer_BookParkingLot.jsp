<!DOCTYPE html>
<%@page import="controller.ResidentParkingLotManager"%>
<%@page import="com.mysql.jdbc.Statement"%>
<%@page import="data.Database"%>
<%@page import="controller.ResidentAreaManager"%>
<%@page import="java.sql.Connection"%>

<html lang="de">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<title>Parkee - Parkplatz buchen</title>
	
	<!-- CSS-Files -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- Page level plugin CSS-->
	<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
	<!-- Custom styles for this template-->
	<link href="vendor/css/sb-admin.css" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
	<!-- checks if the user is logged in as owner -->
	<jsp:include page="customer_CheckLogin.jsp"></jsp:include>

	<!-- Navigation-->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top"
		id="mainNav">
		<a class="navbar-brand" href="customer_Index.jsp">Parkee</a>
		<button class="navbar-toggler navbar-toggler-right" type="button"
			data-toggle="collapse" data-target="#navbarResponsive"
			aria-controls="navbarResponsive" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="Dashboard"><a class="nav-link" href="customer_Index.jsp">
						<i class="fa fa-fw fa-dashboard"></i> <span class="nav-link-text">Dashboard</span>
				</a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="BookParkingLot"><a class="nav-link"
					href="customer_BookParkingLot.jsp"> <i class="fa fa-fw fa-table"></i>
						<span class="nav-link-text">Parkplatz buchen</span>
				</a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="MyBookings"><a class="nav-link"
					href="customer_Bookings.jsp"> <i class="fa fa-fw fa-table"></i>
						<span class="nav-link-text">Meine Buchungen</span>
				</a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="MyVehicles"><a class="nav-link"
					href="customer_Vehicles.jsp"> <i class="fa fa-fw fa-table"></i> <span
						class="nav-link-text">Meine Fahrzeuge</span>
				</a></li>
				<li class="nav-item" data-toggle="tooltip" data-placement="right"
					title="MyProfile"><a class="nav-link"
					href="customer_Profile.jsp"> <i class="fa fa-fw fa-wrench"></i> <span
						class="nav-link-text">Mein Profil</span>
				</a></li>
			</ul>
			<ul class="navbar-nav sidenav-toggler">
				<li class="nav-item"><a class="nav-link text-center"
					id="sidenavToggler"> <i class="fa fa-fw fa-angle-left"></i>
				</a></li>
			</ul>
			<ul class="navbar-nav ml-auto">
				
				<li class="nav-item"><a class="nav-link" data-toggle="modal"
					data-target="#exampleModal"> <i class="fa fa-fw fa-sign-out"></i>Logout
				</a></li>
			</ul>
		</div>
	</nav>

	<div class="content-wrapper">
		<div class="container-fluid">
			<!-- Breadcrumbs-->
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="customer_Index.jsp">Dashboard</a>
				</li>
				<li class="breadcrumb-item active">Parkplatz buchen</li>
			</ol>
			<br>
			
			<p><b>Der gesamte Buchungsvorgang ist aufgrund des Demo-Status von Parkee weder real, 
			noch in irgendeiner Form rechtlich g�ltig.  Es m�ssen keine Zahlungsinformationen angegeben werden.</b></p>
			<br>
		
			<!-- booking ParkingArea search -->
			<form action="customer_BookParkingLotList.jsp">
				<label for="suche" style="font-size: 22px">Parkplatz/Parkbereich ausw�hlen:</label>
				<p><br>
				<input type="search" id="postcode" name="postcode" placeholder="Postleitzahl eingeben" style="margin-left: 5px">
				<p>
				<input type="button" class="btn btn-info" value="Seite zur&uuml;ck" onClick="history.go(-1);return true;">
				<span style="padding: 10px"></span>
				<button type="submit" class="btn btn-success">Parkplatz suchen</button>
			</form>
			
			<!-- Scroll to Top Button-->
			<a class="scroll-to-top rounded" href="#page-top"> <i
				class="fa fa-angle-up"></i>
			</a>
			
			<!-- Logout Modal-->
			<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Ausloggen?</h5>
							<button class="close" type="button" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true"></span>
							</button>
						</div>
						<div class="modal-body">Wollen Sie sich ausloggen?</div>
						<div class="modal-footer">
							<button class="btn btn-secondary" type="button"
								data-dismiss="modal">Abbrechen</button>
							<a class="btn btn-primary" href="logout.jsp">Ausloggen</a>
						</div>
					</div>
				</div>
			</div>
			
			<!-- Bootstrap core JavaScript-->
			<script src="vendor/jquery/jquery.min.js" type="text/javascript"></script>
			<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"
				type="text/javascript"></script>
				
			<!-- Core plugin JavaScript-->
			<script src="vendor/jquery-easing/jquery.easing.min.js" type="text/javascript"></script>
				
			<!-- Page level plugin JavaScript-->
			<script src="vendor/chart.js/Chart.min.js" type="text/javascript"></script>
			<script src="vendor/datatables/jquery.dataTables.js" type="text/javascript"></script>
			<script src="vendor/datatables/dataTables.bootstrap4.js" type="text/javascript"></script>
			
			<!-- Custom scripts for all pages-->
			<script src="vendor/js/sb-admin.min.js" type="text/javascript"></script>
			
			<!-- Custom scripts for this page-->
			<script src="vendor/js/sb-admin-datatables.min.js" type="text/javascript"></script>
			<script src="vendor/js/sb-admin-charts.min.js" type="text/javascript"></script>
		</div>
	</div>
</body>

</html>