<%@page import="controller.LoginManager"%>
<%@page import="model.ParkeeUser"%>
<%@page import="model.Customer"%>
<%@page import="model.Owner"%>

<!DOCTYPE HTML>

<!-- Bootstrap core JavaScript-->
<script src="vendor/jquery/jquery.min.js" type="text/javascript"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js" type="text/javascript"></script>
<!-- Core plugin JavaScript-->
<script src="vendor/jquery-easing/jquery.easing.min.js" type="text/javascript"></script>
<!-- Page level plugin JavaScript-->
<script src="vendor/chart.js/Chart.min.js" type="text/javascript"></script>
<script src="vendor/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="vendor/datatables/dataTables.bootstrap4.js" type="text/javascript"></script>
<!-- Custom scripts for all pages-->
<script src="vendor/js/sb-admin.min.js" type="text/javascript"></script>
<!-- Custom scripts for this page-->
<script src="vendor/js/sb-admin-datatables.min.js" type="text/javascript"></script>
<script src="vendor/js/sb-admin-charts.min.js" type="text/javascript"></script>

<!-- Einbinden CSS-Files -->
<link href="css/stylish-portfolio.css" rel="stylesheet" type="text/css">
<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Custom fonts for this template-->
<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<!-- Page level plugin CSS-->
<link href="vendor/datatables/dataTables.bootstrap4.css" rel="stylesheet">
<!-- Custom styles for this template-->
<link href="vendor/css/sb-admin.css" rel="stylesheet">

<%
	String email = request.getParameter("email");
	String passwd = request.getParameter("passwd");
	if (request.getParameter("passwd") == null) {
		passwd = request.getParameter("passwd1");
	} else {
		passwd = request.getParameter("passwd");
	}

	Customer customer = null;
	Owner owner = null;

	LoginManager loginManager = new LoginManager();

	customer = loginManager.loginCustomer(email, passwd);
	owner = loginManager.loginOwner(email, passwd);

	if (customer == null && owner == null) {
	%>
		<div class="card mb-3">
			<h1 style="color: red">
				<strong>Achtung!</strong> Dieser Benutzer existiert nicht!
			</h1>
			<p style="font-size: 20">Geben Sie Ihre E-mail und Ihr Passwort erneut ein.</p>
			<button type="button" class="btn btn-primary btn-lg"
				onClick="self.location.href='login_signup.jsp'">Zur�ck zum Login!</button>	
			<br> 
			<img src="img/user_not_found.jpg" alt="">
		</div>
	<%
	} else if (customer != null) {
		request.getSession().setAttribute("customer", customer);
		%>
			<jsp:forward page="customer_Index.jsp"></jsp:forward>
		<%
	} else if (owner != null) {
		request.getSession().setAttribute("owner", owner);
		%>
			<jsp:forward page="owner_Index.jsp"></jsp:forward>
		<%
	}
%>
