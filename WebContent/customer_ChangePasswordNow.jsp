<%@page import="controller.CustomerManager"%>
<%@page import="model.Customer"%>

<html>
<head>
	<script src="vendor/jquery/jquery.js" type="text/javascript"></script>
	<link href="vendor/css/stylish-portfolio.css" rel="stylesheet" type="text/css">
</head>

<jsp:include page="customer_CheckLogin.jsp"></jsp:include>
 
<%	
	Customer customer = (Customer) request.getSession().getAttribute("customer");
	String newPassword = request.getParameter("passwd");
		
	CustomerManager cm = new CustomerManager();
	cm.updateCustomerPassword(customer, newPassword);
%>

<jsp:forward page="customer_Profile.jsp"></jsp:forward>