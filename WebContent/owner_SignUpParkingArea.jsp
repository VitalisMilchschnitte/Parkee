<%@page import="controller.ResidentAreaManager"%>
<%@page import="model.Owner"%>

<!DOCTYPE HTML>
<link href="css/stylish-portfolio.css" rel="stylesheet" type="text/css">

<!-- checks if the user is logged in as owner -->
<jsp:include page="owner_CheckLogin.jsp"></jsp:include>

<%
	String areaNumberInput = request.getParameter("areaNumber");
	String areaName = request.getParameter("areaName");
	String parkingSpacesInput = request.getParameter("parkingSpaces");
	String residentParkingLotIDinput = request.getParameter("parkingLotID");

	try {
		int areaNumber = Integer.parseInt(areaNumberInput);
		int parkingSpaces = Integer.parseInt(parkingSpacesInput);
		int residentParkingLotID = Integer.parseInt(residentParkingLotIDinput);

		ResidentAreaManager ram = new ResidentAreaManager();
		ram.registerResidentArea(residentParkingLotID, areaNumber, areaName, parkingSpaces);
	} catch (Exception e) {
		e.printStackTrace();
	}
%>

<jsp:forward page="owner_ParkingLotAreas.jsp"></jsp:forward>