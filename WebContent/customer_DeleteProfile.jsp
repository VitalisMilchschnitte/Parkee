<%@page import="qrCode.QRCodeDeleter"%>
<%@page import="controller.CustomerManager"%>
<%@page import="model.Customer"%>

<%
	Customer customer = (Customer) request.getSession().getAttribute("customer");
	CustomerManager cm = new CustomerManager();
	cm.deleteCustomer(customer);
	
	QRCodeDeleter qrCD = new QRCodeDeleter();
	qrCD.deleteQRCode(customer);
%>

<jsp:forward page="logout.jsp"></jsp:forward>