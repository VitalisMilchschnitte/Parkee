<html lang="de">
<head>
	<meta charset="UTF-8" />
	
	<title>Parkee - Fahrzeug registrieren</title>
	
	<!-- CSS-Files -->
	<link href="vendor/bootstrap/css/bootstrap.css" rel="stylesheet">
	<!-- Custom fonts for this template-->
	<link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script src="vendor/jquery/jquery.js" type="text/javascript"></script>
	<script src="vendor/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
	<!-- Custom styles for this template-->
	<link href="vendor/css/sb-admin.css" rel="stylesheet">
	<link href="vendor/css/vehicleForm.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,
	500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
</head>

<body>
	<div class="container">
		<!-- checks if the user is logged in as customer -->
		<jsp:include page="customer_CheckLogin.jsp"></jsp:include>
		
		<section>
			<div id="container_demo">
				<!-- hidden anchor to stop jump http://www.css3create.com/Astuce-Empecher-le-scroll-avec-l-utilisation-de-target#wrap4  -->
				<a class="hiddenanchor"></a>
				<div id="wrapper">
					<div id="signupVehicle" class="animate form">
						<form action="customer_SignUpVehicle.jsp" autocomplete="on"
							method="post">
							<h1>Fahrzeug registrieren</h1>
							<p>W�hlen Sie Ihre Angaben bitte so, dass ihr Fahrzeug von
								den Kontrolleuren zweifelsfrei identifiziert werden kann.</p>
							<p>
								<label for="licensePlate" class="licensePlate">Kennzeichen</label>
								<input id="licensePlate" name="licensePlate" required="required"
									type="text" placeholder="..." />
							</p>
							<p>
								<label for="vehicleType" class="vehicleType">Fahrzeug-Typ</label>
								<input id="vehicleType" name="vehicleType" required="required"
									type="text" placeholder="Bitte Marke und Modell angeben." />
							</p>
							<p class="VehicleSignUpButton">
								<input type="submit" value="Fahrzeug registrieren"
									target="_blank"><a></a>
							</p>
						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
</body>

</html>