<%@page import="model.Owner"%>
<%@page import="controller.OwnerManager"%>

<html>
<head>
	<script src="vendor/jquery/jquery.js" type="text/javascript"></script>
	<link href="vendor/css/stylish-portfolio.css" rel="stylesheet" type="text/css">
</head>

<jsp:include page="owner_CheckLogin.jsp"></jsp:include>

<%
	Owner owner = (Owner) request.getSession().getAttribute("owner");
	String name = request.getParameter("name");
	String email = request.getParameter("email");

	OwnerManager om = new OwnerManager();
	Owner newOwner = om.updateOwner(owner, name, email);
	request.getSession().setAttribute("owner", newOwner);
%>

<jsp:forward page="owner_Profile.jsp"></jsp:forward>

</html>