package testController;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Test;

import controller.ResidentBookingManager;
import model.Customer;
import model.Owner;
import model.ResidentArea;
import model.ResidentBooking;
import model.Vehicle;

public class TestResidentBookingManager {
	
	ResidentBookingManager rbm = new ResidentBookingManager();

	@Test
	public void testReadResidentBookingsByCustomer() {
		Customer customer = new Customer();
		customer.setId(1000001);
		
		ArrayList<ResidentBooking> residentBookings = rbm.readResidentBookings(customer);
		assertEquals(residentBookings.size(), 3);
	}
	
	@Test
	public void testReadResidentBookingsByOwner() {
		Owner owner = new Owner();
		owner.setId(2);
		
		ArrayList<ResidentBooking> residentBookings = rbm.readResidentBookings(owner);
		assertEquals(residentBookings.size(), 7);
	}
	
	@Test
	public void testReadResidentBookingsByResidentArea() {
		ResidentArea residentArea = new ResidentArea();
		residentArea.setId(200000);
		
		ArrayList<ResidentBooking> residentBookings = rbm.readResidentBookings(residentArea);
		assertEquals(residentBookings.size(), 5);
	}

	@Test
	public void testBookResidentArea() {
		Customer customer = new Customer();
		customer.setId(1000000);
		Vehicle vehicle = new Vehicle();
		vehicle.setId(10000002);
		ResidentArea residentArea = new ResidentArea();
		residentArea.setId(200000);
		String paymentMethod = "PayPal";
		
		rbm.bookResidentAreaForOneWeek(customer, vehicle, residentArea, paymentMethod);
		rbm.bookResidentAreaForOneMonth(customer, vehicle, residentArea, paymentMethod);
		rbm.bookResidentAreaForOneYear(customer, vehicle, residentArea, paymentMethod);
	}
	
}
