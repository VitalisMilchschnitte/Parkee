package testController;

import static org.junit.Assert.*;

import org.junit.Test;

import controller.LoginManager;
import model.Customer;
import model.Owner;

public class TestLoginManager {
	
	LoginManager lm = new LoginManager();

	@Test
	public void testLoginCustomer() {
		String email = "testcustomer@test.com";
		String password = "lala12";
		
		Customer customer = lm.loginCustomer(email, password);
		
		assertEquals(customer.getId(), 1000000);
		assertEquals(customer.getName(), "TestCustomer");
		assertEquals(customer.getEmail(), "testcustomer@test.com");
		assertEquals(customer.getPasswd(), "lala12");
	}
	
	@Test
	public void testLoginOwner() {
		String email = "testowner@test.com";
		String password = "lala12";
		
		Owner owner = lm.loginOwner(email, password);
		
		assertEquals(owner.getId(), 2);
		assertEquals(owner.getName(), "testowner");
		assertEquals(owner.getEmail(), "testowner@test.com");
		assertEquals(owner.getPasswd(), "lala12");
	}

}
