package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import data.Database;
import model.Customer;
import model.Vehicle;

//
/**
 * includes methods to sign & delete vehicles or to read & change its data
 * @author fabianM
 *
 */
public class VehicleManager {
	
	/**
	 * registers vehicle to database
	 * @param customer
	 * @param licensePlate
	 * @param vehicleType
	 */
	public void registerVehicle(Customer customer, String licensePlate, String vehicleType) {
		
		if (customer != null && licensePlate != null && vehicleType != null) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					//creates new vehicle ID
					int id = 10000000;
					int maxID = 0;
					String readMaxIdSQL = "SELECT max(VehicleID) "
									    + "FROM Vehicle";
					rs = stm.executeQuery(readMaxIdSQL);
					while (rs.next()) {
						maxID = rs.getInt("max(VehicleID)");
					}
					if (maxID >= id) {
						id = maxID + 1;
					}
					
					//inserts vehicle into database
					String insertVehicleSQL = "INSERT INTO Vehicle(VehicleID, CustomerID, "
											+ "LicensePlate, VehicleType) "
											+ "VALUES (" + id + ", " + customer.getId() + ", "
											+ "'" + licensePlate + "', '" + vehicleType + "')";
					stm.executeUpdate(insertVehicleSQL);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
					try {rs.close();} catch (SQLException e) {}
				}
			}
		}
	}
	
	/**
	 * checks if license plate is still used in database
	 * @param licensePlate
	 * @return
	 */
	public boolean isLicensePlateUsed(String licensePlate) {
		if (licensePlate != null) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					String readVehicleSQL = "SELECT LicensePlate "
										  + "FROM Vehicle "
										  + "WHERE LicensePlate = '" + licensePlate + "'";
					rs = stm.executeQuery(readVehicleSQL);
					if (rs.next() == true) {
						return true;
					} else {
						return false;
					} 
				}
				catch(SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
					try {rs.close();} catch (SQLException e) {}
				}
			}
		}
		return false;
	}
	
	
	/**
	 * reads out vehicle from database and returns its object
	 * @param vehicleID
	 * @return
	 */
	public Vehicle readVehicleByID(int vehicleID) {
		
		if (vehicleID != 0) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					String readVehicleSQL = "SELECT CustomerID, LicensePlate, VehicleType "
										  + "FROM Vehicle "
										  + "WHERE VehicleID = " + vehicleID;
					rs = stm.executeQuery(readVehicleSQL);
					while (rs.next()) {
						Vehicle vehicle = new Vehicle();
						
						//sets main data
						vehicle.setId(vehicleID);
						vehicle.setLicensePlate(rs.getString("LicensePlate"));
						vehicle.setVehicleType(rs.getString("VehicleType"));
						
						return vehicle;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} 
			}
		}
		return null;
	}
	
	/**
	 * reads out customers registrated vehicles from database
	 * returns an array with them
	 * @param customer
	 * @return
	 */
	public ArrayList<Vehicle> readVehicles(Customer customer) {
		
		if (customer != null) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					ArrayList<Vehicle> vehicles = new ArrayList<>();
					
					String readVehicleSQL = "SELECT VehicleID, LicensePlate, VehicleType "
										  + "FROM Vehicle "
										  + "WHERE CustomerID = " + customer.getId();
					rs = stm.executeQuery(readVehicleSQL);
					while (rs.next()) {
						Vehicle vehicle = new Vehicle();
						
						//sets main data
						vehicle.setId(rs.getInt("VehicleID"));
						vehicle.setLicensePlate(rs.getString("LicensePlate"));
						vehicle.setVehicleType(rs.getString("VehicleType"));
						
						vehicles.add(vehicle);
					}
					
					return vehicles;
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
					try {rs.close();} catch (SQLException e) {}
				}
			}
		}
		return null;
	}
	
	/**
	 * deletes vehicle and it�s data from database
	 * @param vehicle
	 */
	public void deleteVehicleByID(int vehicleID) {
		
		if (vehicleID != 0) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					String deleteVehicleSQL = "DELETE FROM Vehicle "
											+ "WHERE VehicleID = " + vehicleID;
					stm.executeUpdate(deleteVehicleSQL);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
	}
	
	/**
	 * deletes all of customer�s vehicles and their data from database
	 * @param customer
	 */
	public void deleteAllVehicles(Customer customer) {
		if (customer != null) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					String deleteVehicleSQL = "DELETE FROM Vehicle "
											+ "WHERE CustomerID = " + customer.getId();
					stm.executeUpdate(deleteVehicleSQL);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

}