package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import data.Database;
import model.Owner;

/**
 * 
 * @author fabianM
 *
 */
public class OwnerManager {
	
	/**
	 * reads out owner from database and returns its object 
	 * @param ownerID
	 * @return
	 */
	public Owner readOwnerByID(int ownerID) {
		
		if (ownerID != 0) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					String readOwnerSQL = "SELECT OwnerName, Email, Password "
										+ "FROM Owner "
										+ "WHERE OwnerID = " + ownerID;
					rs = stm.executeQuery(readOwnerSQL);
					while (rs.next()) {
						//sets main data
						Owner owner = new Owner();
						owner.setId(ownerID);
						owner.setName(rs.getString("OwnerName"));
						owner.setEmail(rs.getString("Email"));
						owner.setPasswd(rs.getString("Password"));
						
						//sets data of the resident parking lots of the owner
						ResidentParkingLotManager rplm = new ResidentParkingLotManager();
						owner.setResidentParkingLots(rplm.readResidentParkingLots(owner));
						
						return owner;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
					try {rs.close();} catch (SQLException e) {}
				}
			}
		}
		return null;
	}
	
	
	/**
	 * updates owner's data in database
	 * returns user so his new data could set as session attribute 
	 * @param owner
	 * @param name
	 * @param email
	 * @param passwd
	 * @return
	 */
	public Owner updateOwner(Owner owner, String name, String email) {
		
		if (owner != null && name != null && email != null) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					//updates owner�s data in database
					String updateOwnerSQL = "UPDATE Owner "
										  + "SET OwnerName = '" + name + "', "
										  + "Email = '" + email + "' "
										  + "WHERE OwnerID = " + owner.getId();
					stm.executeUpdate(updateOwnerSQL);
					
					//updates owner�s object
					owner.setId(owner.getId());
					owner.setName(name);
					owner.setEmail(email);
					
					return owner;
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
		return null;
	}
	
	/**
	 * updates owner's password in database
	 * returns user so his new data could set as session attribute 
	 * @param owner 
	 * @param newPassword
	 * @return
	 */
	public Owner updateOwnerPassword(Owner owner, String newPassword) {
		
		if (newPassword != null) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					//updates customer�s data in database
					String updateOwnerSQL = "UPDATE Owner "
							  			  + "SET Password = '" + newPassword + "' "
										  + "WHERE OwnerID = " + owner.getId();
					stm.executeUpdate(updateOwnerSQL);
					
					//updates customer object�s data
					owner = readOwnerByID(owner.getId());

					return owner;
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
		return null;
	}

	/**
	 * deletes owner and it�s data from database
	 * @param owner
	 */
	public void deleteOwner(Owner owner) {
		
		if (owner != null) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					String deleteOwnerSQL = "DELETE FROM Owner "
										  + "WHERE OwnerID = " + owner.getId();
					stm.executeUpdate(deleteOwnerSQL);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
	}
	
}
