package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import data.Database;
import model.Customer;
import model.Owner;
import model.ResidentArea;
import model.ResidentBooking;
import model.Vehicle;

public class ResidentBookingManager {
	
	/**
	 * creates a resident booking and inserts it into database
	 * @param customer
	 * @param vehicle
	 * @param residentArea
	 * @param paymentAmount
	 * @param paymentMethod
	 */
	private void createResidentBooking (Customer customer, Vehicle vehicle, String period,
			ResidentArea residentArea, String paymentMethod) {
		
		if (customer != null && vehicle != null && residentArea != null 
				&& paymentMethod != null) {
			Statement stm = Database.getStatement();
			if (stm != null) {
				try {
					//creates new resident booking id
					int id = 100000000;
					int maxID = 0;
					String readMaxIdSQL = "SELECT max(BookingID) "
										+ "FROM Booking";
					ResultSet rs = stm.executeQuery(readMaxIdSQL);
					while (rs.next()) {
						maxID = rs.getInt("max(BookingID)");
					}
					if (maxID >= id) {
						id = maxID + 1;
					}
					
					//creates current date
					LocalDate currentDate = LocalDate.now();
					String currentDateString = currentDate.getDayOfMonth() + "." + currentDate.getMonthValue() + "." + currentDate.getYear();
					
					//creates payment amount and booking end date
					double paymentAmount = 0;
					String endDate = "";
					
					if (period.equals("year")) {
						paymentAmount = 49.99;
						LocalDate currentDateInOneYear = currentDate.plusYears(1);
						endDate = currentDateInOneYear.getDayOfMonth() + "." + currentDateInOneYear.getMonthValue() + "." + currentDateInOneYear.getYear();
					} else if (period.equals("month")) {
						paymentAmount = 9.99;
						LocalDate currentDateInOneMonth = currentDate.plusMonths(1);
						endDate = currentDateInOneMonth.getDayOfMonth() + "." + currentDateInOneMonth.getMonthValue() + "." + currentDateInOneMonth.getYear();
					} else if (period.equals("week")) {
						paymentAmount = 4.99;
						LocalDate currentDateInOneWeek = currentDate.plusWeeks(1);
						endDate = currentDateInOneWeek.getDayOfMonth() + "." + currentDateInOneWeek.getMonthValue() + "." + currentDateInOneWeek.getYear();
					}
					
					
					//inserts resident booking into database table Booking
					String insertBookingSQL = "INSERT INTO Booking "
										    + "(BookingID, CustomerID, "
										    + "VehicleID, Date, PaymentAmount, PaymentMethod) "
										    + "VALUES (" + id + ", "
										    + customer.getId() + ", "
										    + vehicle.getId() + ", "
										    + "'" + currentDateString + "', "
										    + "'" + paymentAmount + "', "
										    + "'" + paymentMethod + "')";
					stm.executeUpdate(insertBookingSQL);
					
					//inserts resident booking into database table ResidentParkingLotBooking
					String insertResidentBookingSQL = "INSERT INTO ResidentParkingLotBooking "
												    + "(BookingID, ResidentParkingLotAreaID, "
												    + "StartDate, EndDate) "
												    + "VALUES (" + id + ", "
												    + residentArea.getId() + ", "
												    + "'" + currentDateString + "', "
												    + "'" + endDate + "')";
					stm.executeUpdate(insertResidentBookingSQL);
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
				}
			}
		}
	}
	
	/**
	 * books a resident area for the customer for one year
	 * @param customer
	 * @param vehicle
	 * @param residentArea
	 * @param paymentMethod
	 */
	public void bookResidentAreaForOneYear(Customer customer, Vehicle vehicle, 
									ResidentArea residentArea, String paymentMethod) {
		String period = "year";
		createResidentBooking(customer, vehicle, period, residentArea, paymentMethod);
	}
	
	/**
	 * books a resident area for the customer for one month
	 * @param customer
	 * @param vehicle
	 * @param residentArea
	 * @param paymentMethod
	 */
	public void bookResidentAreaForOneMonth(Customer customer, Vehicle vehicle, 
									ResidentArea residentArea, String paymentMethod) {
		String period = "month";
		createResidentBooking(customer, vehicle, period, residentArea, paymentMethod);
	}

	/**
	 * books a resident area for the customer for one week
	 * @param customer
	 * @param vehicle
	 * @param residentArea
	 * @param paymentMethod
	 */
	public void bookResidentAreaForOneWeek(Customer customer, Vehicle vehicle, 
									ResidentArea residentArea, String paymentMethod) {
		String period = "week";
		createResidentBooking(customer, vehicle, period, residentArea, paymentMethod);
	}
	
	/**
	 * reads out customer�s resident bookings from database
	 * returns an array with them 
	 * @param customer
	 * @return
	 * @throws SQLException 
	 */
	public ArrayList<ResidentBooking> readResidentBookings(Customer customer){
		if (customer != null) {
			ResultSet rs1 = null;
			Statement stm1 = Database.getStatement();
			if (stm1 != null) {
				try {
					ArrayList<ResidentBooking> residentBookings = new ArrayList<>();
					
					//reads data from table booking
					String readBookingSQL = "SELECT BookingID, CustomerID, VehicleID, "
										  + "Date, PaymentAmount, PaymentMethod "
										  + "FROM Booking "
										  + "WHERE CustomerID "
										  + "= " + customer.getId();
					rs1 = stm1.executeQuery(readBookingSQL);
					while (rs1.next()) {
						Statement stm2 = Database.getStatement();
						ResidentBooking residentBooking = new ResidentBooking();
						
						//sets vehicle�s data
						VehicleManager vm = new VehicleManager();
						Vehicle vehicle = vm.readVehicleByID(rs1.getInt("VehicleID"));
						
						//sets booking�s data
						residentBooking.setId(rs1.getInt("BookingID"));
						residentBooking.setVehicle(vehicle); 
						residentBooking.setDate(rs1.getString("Date"));
						residentBooking.setPaymentAmount(rs1.getDouble("PaymentAmount"));
						residentBooking.setPaymentMethod(rs1.getString("PaymentMethod"));
						
						//reads data from table resident booking
						String readResidentBookingSQL = "SELECT ResidentParkingLotAreaID, "
													  + "StartDate, EndDate "
													  + "FROM ResidentParkingLotBooking "
													  + "WHERE BookingID "
													  + "= " + residentBooking.getId();
						ResultSet rs2 = stm2.executeQuery(readResidentBookingSQL);
						while (rs2.next()) {
							//sets resident area data
							ResidentAreaManager ram = new ResidentAreaManager();
							ResidentArea residentArea 
							= ram.readResidentAreaByID(rs2.getInt("ResidentParkingLotAreaID"));
							
							//sets resident booking�s data
							residentBooking.setResidentArea(residentArea);
							residentBooking.setStartDate(rs2.getString("StartDate"));
							residentBooking.setEndDate(rs2.getString("EndDate"));
						}
						
						residentBookings.add(residentBooking);
					}
					return residentBookings;
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm1.close();} catch (SQLException e) {}
					try {rs1.close();} catch (SQLException e) {}
				}
			}
		}
		
		return null;
	}
	
	/**
	 * reads out owner�s resident bookings from database
	 * returns an array with them 
	 * @param owner
	 * @return
	 */
	public ArrayList<ResidentBooking> readResidentBookings(Owner owner) {
		
		if (owner != null) {
			ResultSet rs1 = null;
			Statement stm1 = Database.getStatement();
			if (stm1 != null) {
				try {
					ArrayList<ResidentBooking> residentBookings = new ArrayList<>();
					
					//reads data from table resident booking
					String readResidentBookingSQL = "SELECT BookingID, ResidentParkingLotAreaID, "
												  + "StartDate, EndDate "
												  + "FROM ResidentParkingLotBooking "
												  + "WHERE ResidentParkingLotAreaID IN "
												  		+ "("
												  		+ "SELECT ResidentParkingLotAreaID "
												  		+ "FROM ResidentParkingLotArea "
												  		+ "WHERE ResidentParkingLotID IN "
												  			+ "("
												  			+ "SELECT ResidentParkingLotID "
												  			+ "FROM ResidentParkingLot "
												  			+ "WHERE OwnerID = " + owner.getId()
												  			+ ")"
												  		+ ")";
					rs1 = stm1.executeQuery(readResidentBookingSQL);
					while (rs1.next()) {
						try {
							Statement stm2 = Database.getStatement();
							ResidentBooking residentBooking = new ResidentBooking();
							
							//sets resident booking�s data
							residentBooking.setId(rs1.getInt("BookingID"));
							residentBooking.setStartDate(rs1.getString("StartDate"));
							residentBooking.setEndDate(rs1.getString("EndDate"));
							
							//set�s parking area�s data
							ResidentAreaManager ram = new ResidentAreaManager();
							residentBooking.setResidentArea(ram.readResidentAreaByID(rs1.getInt("ResidentParkingLotAreaID")));
							
							//reads data from table booking
							String readBookingSQL = "SELECT CustomerID, VehicleID, "
												  + "Date, PaymentAmount, PaymentMethod "
												  + "FROM Booking "
												  + "WHERE BookingID "
												  + "= " + residentBooking.getId();
							ResultSet rs2 = stm2.executeQuery(readBookingSQL);
							while (rs2.next()) {							
								//sets booking�s data
								residentBooking.setDate(rs2.getString("Date"));
								residentBooking.setPaymentAmount(rs2.getDouble("PaymentAmount"));
								residentBooking.setPaymentMethod(rs2.getString("PaymentMethod"));
								
								//set�s customer�s data
								CustomerManager cm = new CustomerManager();
								residentBooking.setCustomer(cm.readCustomerByID(rs2.getInt("CustomerID")));
							}
							
							residentBookings.add(residentBooking);
						} catch (SQLException e) {
							e.printStackTrace();
						} 
					}
					return residentBookings;
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm1.close();} catch (SQLException e) {}
					try {rs1.close();} catch (SQLException e) {}
				}
			}
		}
		return null;
	}
	
	/**
	 * reads out all resident bookings booked on a resident area from database
	 * returns an array with them 
	 * @param residentArea
	 * @return
	 */
	public ArrayList<ResidentBooking> readResidentBookings(ResidentArea residentArea){
		
		if (residentArea != null) {
			Statement stm1 = Database.getStatement();
			ResultSet rs1 = null;
			if (stm1 != null) {
				try {
					ArrayList<ResidentBooking> residentBookings = new ArrayList<>();
					
					String readResidentBookingSQL = "SELECT BookingID, "
												  + "StartDate, EndDate "
												  + "FROM ResidentParkingLotBooking "
												  + "WHERE ResidentParkingLotAreaID "
												  + "= " + residentArea.getId();
					rs1 = stm1.executeQuery(readResidentBookingSQL);
					while (rs1.next()) {
						Statement stm2 = Database.getStatement();
						ResidentBooking residentBooking = new ResidentBooking();
						
						//sets resident booking�s data
						residentBooking.setId(rs1.getInt("BookingID"));
						residentBooking.setStartDate(rs1.getString("StartDate"));
						residentBooking.setEndDate(rs1.getString("EndDate"));
						
						//reads data from table booking
						String readBookingSQL = "SELECT CustomerID, VehicleID, "
											  + "Date, PaymentAmount, PaymentMethod "
											  + "FROM Booking "
											  + "WHERE BookingID "
											  + "= " + residentBooking.getId();
						ResultSet rs2 = stm2.executeQuery(readBookingSQL);
						while (rs2.next()) {
							//sets customer�s data
							CustomerManager cm = new CustomerManager();
							Customer customer = cm.readCustomerByID(rs2.getInt("CustomerID"));
							
							//sets booking�s data
							residentBooking.setCustomer(customer);
							residentBooking.setDate(rs2.getString("Date"));
							residentBooking.setPaymentAmount(rs2.getDouble("PaymentAmount"));
							residentBooking.setPaymentMethod(rs2.getString("PaymentMethod"));
						}
						
						residentBookings.add(residentBooking);
					}
					return residentBookings;
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm1.close();} catch (SQLException e) {}
					try {rs1.close();} catch (SQLException e) {}
				}
			} 
		}
		return null;
	}
	
}
