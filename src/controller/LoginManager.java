package controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import data.Database;
import model.Customer;
import model.Owner;

/**
 * 
 * @author fabianM
 *
 */
public class LoginManager {
	
	/**
	 * helps Customer to log in with his email and password
	 * checks table Customer for login data
	 * returns user so his data could set as session attribute
	 * @param email
	 * @param passwd
	 * @return Customer
	 */
	public Customer loginCustomer (String email, String passwd) {
		
		if (email != null && passwd != null) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					String readCustomerSQL = "SELECT * "
										   + "FROM Customer "
										   + "WHERE Email = '" + email + "' "
										   + "AND BINARY Password = '" + passwd + "'";
					rs = stm.executeQuery(readCustomerSQL);
					while (rs.next()) {
						Customer customer = new Customer();
						
						//sets main data
						customer.setId(rs.getInt("CustomerID"));
						customer.setName(rs.getString("CustomerName"));
						customer.setEmail(rs.getString("Email"));
						customer.setPasswd(rs.getString("Password"));
						
						//sets data of all vehicles from customer
						VehicleManager vm = new VehicleManager();
						customer.setVehicles(vm.readVehicles(customer));
						
						return customer;
					} 
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
					try {rs.close();} catch (SQLException e) {}
				}
			}
		}
		return null;
	}
	
	/**
	 * helps Owner to log in with his email and password
	 * checks table Owner for login data
	 * returns user so his data could set as session attribute
	 * @param email
	 * @param passwd
	 * @return Owner
	 */
	public Owner loginOwner (String email, String passwd) {
		
		if (email != null && passwd != null) {
			Statement stm = Database.getStatement();
			ResultSet rs = null;
			if (stm != null) {
				try {
					String readOwnerSQL = "SELECT * "
										+ "FROM Owner "
										+ "WHERE Email = '" + email + "' "
										+ "AND BINARY Password = '" + passwd + "'";
					rs = stm.executeQuery(readOwnerSQL);
					while (rs.next()) {
						Owner owner = new Owner();
						
						//sets main data
						owner.setId(rs.getInt("OwnerID"));
						owner.setName(rs.getString("OwnerName"));
						owner.setEmail(rs.getString("Email"));
						owner.setPasswd(rs.getString("Password"));
						
						//sets data of all resident parking lots from owner
						ResidentParkingLotManager rplm = new ResidentParkingLotManager();
						owner.setResidentParkingLots(rplm.readResidentParkingLots(owner));
						
						//sets data of all resident bookings from owner
						ResidentBookingManager rbm = new ResidentBookingManager();
						owner.setResidentBookings(rbm.readResidentBookings(owner));
						
						return owner;
					} 
				} catch (SQLException e) {
					e.printStackTrace();
				} finally {
					try {Database.getConnection().close();} catch (SQLException e) {}
					try {stm.close();} catch (SQLException e) {}
					try {rs.close();} catch (SQLException e) {}
				}
			}
		}
		return null;
	}
	
}
