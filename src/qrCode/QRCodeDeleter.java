package qrCode;

import java.io.File;

import model.Customer;

/**
 * 
 * @author fabianM
 *
 */
public class QRCodeDeleter {
	
	/**
	 * deletes customerīs personalized QR code when his or her account gets deleted
	 * @param customer
	 * @return
	 */
	public void deleteQRCode(Customer customer) {
		File qrCodeFile = new File("D:\\home\\site\\wwwroot\\qr-code\\QRcode-" + customer.getId() + ".png");
		qrCodeFile.delete();
	}
	
}
