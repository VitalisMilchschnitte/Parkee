package qrCode;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Hashtable;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * 
 * @author fabianM
 *
 */
public class QRCodeGenerator {
	
	/**
	 * creates a QR code file in the directory 'QR-Code'
	 * @param customerID
	 */
	public void createQRCodeFile(int customerID) {
		
		String qrCodeURL = "https://parkee.azurewebsites.net/checkQRCode.jsp?id=" + customerID;
		String fileName = "QRcode-" + customerID + ".png";
		String filePath = "D:\\home\\site\\wwwroot\\qr-code\\" + fileName;
		int imageSize = 550;
		String fileType = "png";
		
		File qrFile = new File(filePath);
		try {
			createQRImage(qrFile, qrCodeURL, imageSize, fileType);
		} catch (IOException | WriterException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * creates the image for the QR code file
	 * @param qrFile
	 * @param qrCodeText
	 * @param size
	 * @param fileType
	 * @throws IOException
	 * @throws WriterException
	 */
	private static void createQRImage(File qrFile, String qrCodeText, int size, String fileType) 
			throws IOException, WriterException {
		
		// Create the ByteMatrix for the QR-Code that encodes the given String
		Hashtable hintMap = new Hashtable();
		hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
		QRCodeWriter qrCodeWriter = new QRCodeWriter();
		BitMatrix byteMatrix = qrCodeWriter.encode(qrCodeText, BarcodeFormat.QR_CODE, size, size, hintMap);
		
		// Make the BufferedImage that are to hold the QRCode
		int matrixWidth = byteMatrix.getWidth();
		BufferedImage image = new BufferedImage(matrixWidth, matrixWidth,
				BufferedImage.TYPE_INT_RGB);
		image.createGraphics();
		Graphics2D graphics = (Graphics2D) image.getGraphics();
		graphics.setColor(Color.WHITE);
		graphics.fillRect(0, 0, matrixWidth, matrixWidth);
		
		// Paint and save the image using the ByteMatrix
		graphics.setColor(Color.BLACK);
		for (int i = 0; i < matrixWidth; i++) {
			for (int j = 0; j < matrixWidth; j++) {
				if (byteMatrix.get(i, j)) {
					graphics.fillRect(i, j, 1, 1);
				}
			}
		}
		
		ImageIO.write(image, fileType, qrFile);
	}
	
}
