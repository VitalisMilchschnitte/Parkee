package model;

import java.util.ArrayList;

/**
 * 
 * @author fabianM
 *
 */
public class ResidentParkingLot {
	
	//attributes
	private int id;
	private String postCode;
	private String postCodeDistrict;
	private ArrayList<ResidentArea> residentAreas;
	
	//getters and setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getPostCode() {
		return postCode;
	}
	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}
	
	public String getPostCodeDistrict() {
		return postCodeDistrict;
	}
	public void setPostCodeDistrict(String postCodeDistrict) {
		this.postCodeDistrict = postCodeDistrict;
	}
	
	public ArrayList<ResidentArea> getResidentAreas() {
		return residentAreas;
	}
	public void setResidentAreas(ArrayList<ResidentArea> residentAreas) {
		this.residentAreas = residentAreas;
	}
	
}
