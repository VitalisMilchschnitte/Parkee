package model;

import java.util.ArrayList;

/**
 * 
 * @author fabianM
 *
 */
public class Vehicle {
	
	//attributes
	private int id;
	private String licensePlate;
	private String vehicleType;
	private ArrayList<ResidentBooking> residentBookings = new ArrayList<>();
	
	//getters and setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getLicensePlate() {
		return licensePlate;
	}
	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}
	
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	
	public ArrayList<ResidentBooking> getResidentBookings() {
		return residentBookings;
	}
	public void setResidentBookings(ArrayList<ResidentBooking> residentBookings) {
		this.residentBookings = residentBookings;
	}
	
}
