package model;

import java.util.ArrayList;

/**
 * 
 * @author fabianM
 *
 */
public class Customer extends ParkeeUser {
	
	//attributes
	private ArrayList<Vehicle> vehicles = new ArrayList<>();
	private ArrayList<ResidentBooking> residentBookings = new ArrayList<>();
	
	//getters and setters
	public ArrayList<Vehicle> getVehicles() {
		return vehicles;
	}
	public void setVehicles(ArrayList<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}
	
	public ArrayList<ResidentBooking> getResidentBookings() {
		return residentBookings;
	}
	public void setResidentBookings(ArrayList<ResidentBooking> residentBookings) {
		this.residentBookings = residentBookings;
	}
	
}
